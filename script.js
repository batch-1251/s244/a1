db.users.insertMany([
	{"First Name": "Diane", "Last Name": "Murphy", "Email": "dmurphy@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Mary", "Last Name": "Patterson", "Email": "mpatterson@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Jeff", "Last Name": "Firrelli", "Email": "jfirrelli@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Gerard", "Last Name": "Bondur", "Email": "gbondur@mail.com", "Is Admin": false, "Is Active": true},
	{"First Name": "Pamela", "Last Name": "Castillo", "Email": "pcastillo@mail.com", "Is Admin": true, "Is Active": false},
	{"First Name": "George", "Last Name": "Vanauf", "Email": "gvanauf@mail.com", "Is Admin": true, "Is Active": true},

])

db.courses.insertMany([
	{"Name": "Professional Development", "Price": 10000},
	{"Name": "Business Processing", "Price": 13000}
])


db.courses.updateOne({"Name": "Professional Development"},
			{$set:{"Enrollees":[ObjectId("6125b4e10743609952fae1a5"),ObjectId("6125b4e10743609952fae1a7")]}}
			)

db.courses.updateOne({"Name": "Business Processing"},
			{$set:{"Enrollees":[ObjectId("6125b4e10743609952fae1a8"),ObjectId("6125b4e10743609952fae1a6")]}}
			)


db.users.find({"Is Admin": false})
	